PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/twrp_gale.mk

COMMON_LUNCH_CHOICES := \
    twrp_gale-user \
    twrp_gale-userdebug \
    twrp_gale-eng
